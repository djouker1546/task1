﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class UserContext : DbContext
    {
            public UserContext()
                : base("Task1")
        { }

        public DbSet<User> Users { get; set; }

    }
}
