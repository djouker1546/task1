﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Excel = Microsoft.Office.Interop.Excel;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Xml;

namespace WindowsFormsApp1
{      
    
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
           
            
                var text = "x";
                int numberStr = 0;
            //  Открывает окно выбора файла
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Считывание информации из ффайла и разбиение на объекты
                using (var file = new StreamReader(File.OpenRead(openFileDialog.FileName)))
                using (UserContext db = new UserContext())
                {

                    while (!file.EndOfStream && text != "")
                    {

                        text = file.ReadLine();
                        // Разбиение каждого объекта на свойства
                        var properties = text.Split(';');
                        numberStr++;
                        // Проверка на количество свойств
                        if (properties.Length != 6)
                        {
                            MessageBox.Show($"Заданы не 6 свойств! Ошибка в {numberStr} строке!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            continue;
                        }
                        // Проверка на заданность свойств
                        if (properties.Any(x => string.IsNullOrWhiteSpace(x)))
                        {
                            MessageBox.Show($"Свойства не могут быть пустыми! Ошибка в {numberStr} строке!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            continue;
                        }
                        // Првоерка формата даты
                        var datetime = DateTime.MinValue;
                        if (!DateTime.TryParse(properties[0], out datetime) || datetime.Year < 1900)
                        {
                        MessageBox.Show ($"Не верный формат даты! Ошибка в {numberStr} строке!" ,"Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            continue;
                        }
                        // Создание объектов класса User и занесение в него свойств 
                        User user = new User
                        {
                            Date = datetime,
                            FirstName = properties[1],
                            LastName = properties[2],
                            SurName = properties[3],
                            City = properties[4],
                            Country = properties[5]
                        };
                        // Занесение каждого объекта в БД    
                        db.Users.Add(user);
                            

                    }
                    // Сохранение изменений
                    db.SaveChanges();
                    MessageBox.Show("Файл загружен.", "Импорт", MessageBoxButtons.OK, MessageBoxIcon.Information);
                } 
            }
                
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Переход на форму фильтра
            Filter formFilter = new Filter();
            formFilter.Show();



                    
             
            
            
        }
    }
}
