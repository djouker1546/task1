﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Filter : Form
    {
        public Filter()
        {
            InitializeComponent();
        }

        private void buttonExportToExcel_Click(object sender, EventArgs e)
        {
            // Выбор файла для записи данных
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
                UpdateCell(saveFileDialog.FileName);
        }

        private static WorksheetPart GetWorksheetPartByName(SpreadsheetDocument document, string sheetName)
        {
            IEnumerable<Sheet> sheets = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().
                            Elements<Sheet>().Where(s => s.Name == sheetName);
            if (sheets.Count() == 0)
            {
                return null;
            }
            string relationshipId = sheets.First().Id.Value;
            WorksheetPart worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(relationshipId);
            return worksheetPart;
        }

        public void UpdateCell(string docName)
        {
            var cellType = new EnumValue<CellValues>(CellValues.String);
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Create(docName, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart workbookpart = spreadSheet.AddWorkbookPart();
                workbookpart.Workbook = new Workbook();
                
                WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());

                Sheets sheets = spreadSheet.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());

                // Append a new worksheet and associate it with the workbook.
                Sheet sheet = new Sheet()
                {
                    Id = spreadSheet.WorkbookPart.GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = "Main"
                };
                sheets.Append(sheet);

                if (worksheetPart != null)
                {
                    Worksheet worksheet = worksheetPart.Worksheet;

                    //// Создает новую SheetData  
                    SheetData sheetData = worksheet.GetFirstChild<SheetData>();

                    sheetData.RemoveAllChildren<Row>();

                    uint row1 = 0;
                    // Фильтр; Если в textBox не равен null, то фильтровать по нему(всего 6 textBox)
                    using (UserContext db = new UserContext())
                    {

                        DateTime date = DateTime.MinValue;
                        if (!string.IsNullOrWhiteSpace(textBoxDate.Text) && !DateTime.TryParse(textBoxDate.Text, out date))
                        {
                            MessageBox.Show("Дата введена неверно!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                      //  DateTime date = DateTime.TryParse(textBoxDate.Text, out var dt) ? dt : DateTime.MinValue;
                       // if (textBoxDate.Text != "" && date != DateTime.MinValue){
                        //    MessageBox.Show("Дата введена неверно!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                         //   return;
                          //  }
                        var query = (IQueryable<User>)db.Users;

                        if (!string.IsNullOrWhiteSpace(textBoxDate.Text))
                            query = query.Where(x => x.Date == date);

                        if (!string.IsNullOrWhiteSpace(textBoxFirstName.Text))
                            query = query.Where(x => x.FirstName.Contains(textBoxFirstName.Text));

                        if (!string.IsNullOrWhiteSpace(textBoxLastName.Text))
                            query = query.Where(x => x.LastName.Contains(textBoxLastName.Text));

                        if (!string.IsNullOrWhiteSpace(textBoxSurName.Text))
                            query = query.Where(x => x.SurName.Contains(textBoxSurName.Text));

                        if (!string.IsNullOrWhiteSpace(textBoxCity.Text))
                            query = query.Where(x => x.City.Contains(textBoxCity.Text));

                        if (!string.IsNullOrWhiteSpace(textBoxCountry.Text))
                            query = query.Where(x => x.Country.Contains(textBoxCountry.Text));

                        foreach (User u in query.ToList())
                        {
                            row1++;

                            Row row = new Row() { RowIndex = row1 };
                            row.RemoveAllChildren<Cell>();

                            List<string> listU = new List<string> { u.Date.ToString("dd.MM.yyyy"), u.FirstName, u.LastName, u.SurName, u.City, u.Country };
                            
                            // создает новые ячейки
                            Cell cell = new Cell() { CellReference = "A" + row, DataType = cellType, CellValue = new CellValue(listU[0]) };
                            Cell cell1 = new Cell() { CellReference = "B" + row, DataType = cellType, CellValue = new CellValue(listU[1]) };
                            Cell cell2 = new Cell() { CellReference = "C" + row, DataType = cellType, CellValue = new CellValue(listU[2]) };
                            Cell cell3 = new Cell() { CellReference = "D" + row, DataType = cellType, CellValue = new CellValue(listU[3]) };
                            Cell cell4 = new Cell() { CellReference = "E" + row, DataType = cellType, CellValue = new CellValue(listU[4]) };
                            Cell cell5 = new Cell() { CellReference = "F" + row, DataType = cellType, CellValue = new CellValue(listU[5]) };


                            // Добавляет ячейки в строку
                            row.AppendChild(cell);
                            row.AppendChild(cell1);
                            row.AppendChild(cell2);
                            row.AppendChild(cell3);
                            row.AppendChild(cell4);
                            row.AppendChild(cell5);


                            // Добавляет строку в sheetData
                            sheetData.Append(row);
                        }
                    }
                    spreadSheet.Save();
                }
                MessageBox.Show("Данные экспортированы!", "Экспорт", MessageBoxButtons.OK, MessageBoxIcon.Information);
               // Process.Start(saveFileDialog.FileName);
            }

        }
    }
}
